from rest_framework import serializers

from clinicapp.models import Cita, Especialidades, Horarios, MedicosEspecialidades, Pacientes,Medicos

class PacienteSerializer(serializers.ModelSerializer):
    class Meta:
        model = Pacientes
        fields = ['paciente_id', 'nombre','apellidos','dni','direccion','correo','telefono','sexo', 'fecha_REG','fecha_MOD', 'usuario_REG', 'usuario_MOD','activo']


class MedicoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Medicos
        fields = [ 'medico_id', 'nombre','apellidos','dni','direccion','correo','telefono','sexo', 'fecha_N','fecha_REG','fecha_MOD', 'usuario_REG', 'usuario_MOD','activo','num_colegiatura',]        


class EspecialidadesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Especialidades
        fields = [ 'nombre','descripcion', 'fecha_REG', 'fecha_MOD', 'usuario_REG','usuario_MOD','activo'  ]          

class HorarioSerializer(serializers.ModelSerializer):
    class Meta:
        model = Horarios
        fields = [ 'id','medico_id','fecha_atencion','fin_atencion','activo','fecha_REG','fecha_MOD','usuario_REG','usuario_MOD']      


class MedicoEspecialidadesSerializer(serializers.ModelSerializer):
    class Meta:
        model = MedicosEspecialidades
        fields = [ 'ID','medico_id','especialidades']                  


class CitaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Cita
        fields = [ 'ID','medico_id','fecha_atencion','fin_atencion','paciente_id']            