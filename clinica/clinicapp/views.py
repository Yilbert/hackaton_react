from django.shortcuts import render
from clinicapp import serializers
from rest_framework import viewsets
from clinicapp.models import Pacientes,Medicos,MedicosEspecialidades,Horarios,Especialidades,Cita 
from clinicapp.serializers import CitaSerializer, MedicoEspecialidadesSerializer,PacienteSerializer,MedicoSerializer,HorarioSerializer,EspecialidadesSerializer
# Create your views here.

class PacienteViewSet(viewsets.ModelViewSet):
    queryset = Pacientes.objects.all()
    serializer_class = PacienteSerializer


class MedicoViewSet(viewsets.ModelViewSet):
    queryset = Medicos.objects.all() 
    serializer_class = MedicoSerializer



class MedicoEspecialistaViewSet(viewsets.ModelViewSet):
    queryset = MedicosEspecialidades.objects.all()
    serializer_class = MedicoEspecialidadesSerializer


class HorariosViewset(viewsets.ModelViewSet):
    queryset = Horarios.objects.all()
    serializer_class = HorarioSerializer


class CitasViewset(viewsets.ModelViewSet):
    queryset = Cita.objects.all()
    serializer_class = CitaSerializer


class EspecialidadesViewset(viewsets.ModelViewSet):
    queryset = Especialidades.objects.all()
    serializer_class = EspecialidadesSerializer    