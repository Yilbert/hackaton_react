from django.contrib import admin
from django.urls import path
from rest_framework import routers
from .views import MedicoViewSet,PacienteViewSet,MedicoEspecialistaViewSet,HorariosViewset,CitasViewset,EspecialidadesViewset





router = routers.DefaultRouter()
router.register(r'medico',MedicoViewSet)
router.register(r'paciente',PacienteViewSet)
router.register(r'especialidades',EspecialidadesViewset)
router.register(r'horarios',HorariosViewset)
router.register(r'cita',CitasViewset)
router.register(r'medicoespecilistas',MedicoEspecialistaViewSet)
