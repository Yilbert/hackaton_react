from django.contrib import admin

# Register your models here.

from clinicapp.models import Cita, Especialidades, Horarios, Medicos, MedicosEspecialidades, Pacientes


@admin.register(Medicos)
class MedicoAdmin(admin.ModelAdmin):
    list_display = ('nombre', 'apellidos', 'dni','telefono', 'direccion', 'correo')
    search_fields = ('nombre', 'apellidos')


@admin.register(Pacientes)
class PacienteAdmin(admin.ModelAdmin):
    list_display =('nombre', 'apellidos', 'dni','telefono', 'direccion', 'correo')
    search_fields = ('nombre', 'apellidos')


@admin.register(Especialidades)
class EspecialidadesAdmin(admin.ModelAdmin):
    list_display = ('id', 'nombre', 'descripcion', 'fecha_REG','fecha_MOD', 'usuario_REG', 'usuario_MOD')
    





@admin.register(MedicosEspecialidades)
class MedicosEspecialidadesAdmin(admin.ModelAdmin):
    list_display =('especialidades',)
    



@admin.register(Horarios)
class HorariosAdmin(admin.ModelAdmin):
    list_display =('fecha_atencion',)
    


@admin.register(Cita)
class CitaAdmin(admin.ModelAdmin):
    list_display =('estado',)
    







