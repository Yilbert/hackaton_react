from django.db import models


class PersonaBase(models.Model):
    """
    User base model
    """
    
    nombre = models.CharField(max_length=45)
    apellidos = models.CharField(max_length=45)
    dni = models.CharField(max_length=45)
    direccion = models.CharField(max_length=45)
    correo = models.CharField(max_length=45)
    telefono = models.CharField(max_length=45)
    sexo= models.CharField(max_length=45)
    fecha_N = models.DateTimeField()
    fecha_REG = models.DateTimeField()
    fecha_MOD= models.DateTimeField()
    usuario_REG = models.IntegerField()
    usuario_MOD = models.IntegerField()
    activo= models.IntegerField()
    
    
    class Meta:
        abstract = True


class Especialidades(models.Model):
    
    
    nombre= models.CharField(max_length=45)
    descripcion = models.CharField(max_length=45)
    fecha_REG = models.DateTimeField()
    fecha_MOD = models.DateTimeField()
    usuario_REG = models.IntegerField()
    usuario_MOD= models.IntegerField()
    activo= models.IntegerField()
    
    def _str_(self):
            return ( str('Especialidad: ')+ self.nombre )
    
    

class Medicos(PersonaBase):
    medico_id = models.AutoField(primary_key=True)
    num_colegiatura= models.CharField(max_length=45)
    
    
    def __str__(self):
        return f'{self.nombre} {self.apellidos}'
    
class MedicosEspecialidades(PersonaBase):
    ID = models.IntegerField(primary_key=True)
    especialidades = models.ForeignKey(Especialidades, null =True, blank = False, on_delete= models.CASCADE)
    medico_id= models.ForeignKey(Medicos, null =True, blank = False, on_delete= models.CASCADE)
    especialidades = models.CharField(max_length=45)
    
    
    def _str_(self):
            return ( str('Medico: ')+ self.medico_id.nombre +' / '+ str('Especialidad: ')+ self.especialidades)
    
class Horarios(models.Model):
    id = models.IntegerField(primary_key=True)
    medico_id = models.ForeignKey(Medicos, null =True, blank = False, on_delete= models.CASCADE)
    fecha_atencion = models.DateTimeField()
    fin_atencion = models.DateTimeField()
    activo = models.IntegerField()
    fecha_REG = models.DateTimeField()
    usuario_REG = models.IntegerField()
    fecha_MOD = models.DateTimeField()
    usuario_MOD = models.IntegerField()
    
    def _str_(self):
            return ( str('Medico: ')+ self.medico_id.nombre +' / '+ str('Fecha: ')+ str(self.fecha_atencion))
    
    
class Pacientes(PersonaBase):
    
    paciente_id = models.AutoField(primary_key=True)

    def __str__(self):
        return f'{self.nombre} {self.apellidos}'

class Cita (models.Model):
    ID = models.IntegerField(primary_key=True)
    medico_id = models.ForeignKey(Medicos, null =True, blank = False, on_delete= models.CASCADE)
    paciente_id = models.ForeignKey(Pacientes, null =True, blank = False, on_delete= models.CASCADE)
    fecha_atencion = models.DateTimeField() 
    inicio_atencion = models.DateTimeField()
    fin_atencion = models.DateTimeField()
    estado = models.CharField(max_length=45)
    observacion= models.CharField(max_length=45)
    activo = models.IntegerField()
    fecha_REG = models.DateTimeField()
    usuario_REG = models.IntegerField()
    fecha_MOD = models.DateTimeField()
    usuario_MOD = models.IntegerField()
    
    def _str_(self):
            return ( str('Paciente: ')+ self.paciente_id.nombre +' / '+ str('Apellido: ')+ self.paciente_id.apellidos)
